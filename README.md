# arria10som-manifest

This is the arria10som-manifest repository.

It contains a manifest to setup the **yocto/poky** build system for the **Dream Chip Technologies** arria10som project.

This manifest can be used to clone all necessary git repositories with **Google**'s *repo* tool.

## Preparation

Install or get **Google**'s *repo* tool.

Install:

    $ sudo apt-get install repo

Get:

    $ mkdir -p ~/bin
    $ PATH=~/bin:$PATH

    $ curl http://commondatastorage.googleapis.com/git-repo-downloads/repo > ~/bin/repo
    $ chmod a+x ~/bin/repo


## Usage

Getting a development version:

    $ repo init -u https://gitlab.com/dreamchip/arria10som-manifest.git -b morty -m default.xml

Getting a release version:

    $ repo init -u https://gitlab.com/dreamchip/arria10som-manifest.git -b morty -m release.xml

Sync to clone the repositories:

    $ repo sync

Then source the **yocto/poky** build environment:

    $ source yocto/poky/oe-init-build-env

Now you are in the **yocto/poky** build environment and can do things like:

    $ bitbake dreamchip-arria10som-image-minimal
    $ bitbake dreamchip-arria10som-image-minimal -c populate_sdk
    $ bitbake dreamchip-arria10som-image-minimal -c populate_sdk_ext
    $ bitbake dreamchip-arria10som-image-minimal_dev
    $ bitbake dreamchip-arria10som-image-minimal_dev -c populate_sdk
    $ bitbake dreamchip-arria10som-image-minimal_dev -c populate_sdk_ext
    $ bitbake meta-toolchain
    $ bitbake dreamchip-arria10som-image-xfce
    $ bitbake u-boot-dreamchip-arria10som

## Hints
The created Images could be found under <yourpath>/build/tmp/deploy/images
